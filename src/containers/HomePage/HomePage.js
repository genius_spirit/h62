import React, { Component, Fragment } from 'react';
import Main from "../../components/Main/Main";

class HomePage extends Component {
  render() {
    return(
      <Fragment>
        <Main/>
      </Fragment>
    )
  }
}

export default HomePage;