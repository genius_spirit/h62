import React, {Component, Fragment} from 'react';
import './Works.css';

class Works extends Component {

  CONST = [
    {auto: 'Nissan', model: 'GT-R C10', img: '/images/C10.jpg'},
    {auto: 'Nissan', model: 'GT-R C110', img: '/images/C110.jpg'},
    {auto: 'Nissan', model: 'GT-R R32', img: '/images/R32.jpg'},
    {auto: 'Nissan', model: 'GT-R R33', img: '/images/R33.jpg'},
    {auto: 'Nissan', model: 'GT-R R34', img: '/images/R34.jpg'},
    {auto: 'Nissan', model: 'GT-R R35', img: '/images/R35.jpg'},
    {auto: 'Nissan', model: 'Fairlady Z', img: '/images/Z33.jpg'},
    {auto: 'Nissan', model: 'Laurel 2000 SGX', img: '/images/laurel.jpg'}
  ];


  handleViewProject = (index) => {console.log(index);

    const query = ('?car=' + encodeURIComponent(this.CONST[index].auto) +
                   '&model=' + encodeURIComponent(this.CONST[index].model));

    this.props.history.push({
      pathname: '/works/car',
      search: query
    });
  };

  render() {
    return(
      <Fragment>
        <section className="works">
          { this.CONST.map((item, index)  => {
              return (
                <div key={index} className='card' style={{background: 'url(' + item.img + ') 50% 50% no-repeat', backgroundSize: 'cover'}} onClick={() => this.handleViewProject(index)}>
                  <h3 className='card__title'>{item.car} {item.model}</h3>
                </div>
              )
            })
          }
        </section>
      </Fragment>
    )
  }
}

export default Works;