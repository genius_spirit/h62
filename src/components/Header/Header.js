import React from 'react';
import './Header.css';
import NavLink from "react-router-dom/es/NavLink";

const Header = (props) => {
  return(
    <header className="header">
      <div className="logo"><span className='logo__red'>Burga</span>RacingTeam</div>
      <nav className="mainNav">
        <ul className="mainNav__list">
          <li className="mainNav__item"><NavLink className="mainNav-link" to="/">Домой</NavLink></li>
          <li className="mainNav__item"><NavLink className="mainNav-link" to="/works">Наши проекты</NavLink></li>
          <li className="mainNav__item"><NavLink className="mainNav-link" to="/contacts">Контакты</NavLink></li>
        </ul>
      </nav>
    </header>
  )
};

export default Header;