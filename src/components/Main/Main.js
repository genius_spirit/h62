import React from 'react';
import './Main.css';

const Main = (props) => {
  return(
    <main className="main">
        <h1 className="main__title">Мы сделаем то, что <span className="main__red">не смогут</span> сделать другие.</h1>
    </main>
  )
};

export default Main;