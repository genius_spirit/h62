import React, { Component } from 'react';

class Car extends Component {
  state = {
    currentCar: ''
  };

  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    for (let p of query.entries()) {
      this.setState({currentCar: p[1]})
    }
  }

  render() {

    return(
      <div style={{textAlign: 'center'}}>
         <p>Статья о тюнинге автомобиля {this.state.currentCar} будет  выведена на этой странице скорее всего из БД</p>
      </div>
    )
  }
}

export default Car;