import React, { Component } from 'react';
import HomePage from "./containers/HomePage/HomePage";
import {Route, Switch} from "react-router-dom";
import Works from "./containers/Works/Works";
import Contacts from "./containers/Contacts/Contacts";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Car from "./components/Car/Car";


class App extends Component {
  render() {
    return(
      <div className="container">
        <Header/>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/works" exact component={Works} />
          <Route path="/works/car" component={Car}/>
          <Route path="/contacts" component={Contacts} />
        </Switch>
        <Footer/>
      </div>

    )
  }
}

export default App;